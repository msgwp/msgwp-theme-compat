<?php
/*
 * Plugin Name: msgWP: Theme Compatibility
 * Description: For default WordPress themes…
 * Plugin URI:  https://msgwp.com
 * Version:     0.10.1
 * Author:      implenton
 * Author URI:  https://implenton.com
 * License:     GPLv3
 * License URI: https://www.gnu.org/licenses/gpl-3.0.txt
 */

namespace msgWP\AddOn\ThemeCompat;

use Puc_v4_Factory;

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

register_activation_hook( __FILE__, __NAMESPACE__ . '\plugin_activation' );

require_once dirname( __FILE__ ) . '/vendor/yahnis-elsts/plugin-update-checker/plugin-update-checker.php';

add_filter( 'stylesheet_directory', __NAMESPACE__ . '\overwrite_twentynineteen', 10, 3 );
add_filter( 'post_class', __NAMESPACE__ . '\post_class' );

add_filter( 'the_title', __NAMESPACE__ . '\set_title_from_content', 10, 2 );

add_filter( 'single_post_title', __NAMESPACE__ . '\set_single_post_title_from_content', 10, 2 );
add_filter( 'wpseo_title', __NAMESPACE__ . '\yoast_title' );

function plugin_activation() {
    // TODO: Internationalize the plugin activation errors
    if ( ! defined( 'MSGWP_VERSION' ) ) {
        wp_die( 'You have to install and activate <a href="https://msgwp.com" target="_blank" rel="noopener">msgWP</a> first.', '' );
    }
}

function overwrite_twentynineteen( $stylesheet_dir, $stylesheet, $theme_root ) {
    if ( $stylesheet !== 'twentynineteen' ) {
        return $stylesheet_dir;
    }

    return dirname( __FILE__ ) . '/twentynineteen';
}

function post_class( $classes ) {
    if ( is_msgwp_post() ) {
        $classes[] = 'post-msgwp';
    }

    return $classes;
}

function set_title_from_content( $title, $post_id ) {
    if ( is_admin() ) {
        return $title;
    }

    if ( ! is_uid_title( $title, $post_id ) ) {
        return $title;
    }

    return generate_title_from_content( $post_id );
}

function set_single_post_title_from_content( $title, $post_id ) {
    if ( is_admin() ) {
        return $title;
    }

    if ( is_home() ) {
        return $title;
    }

    if ( is_user_title() ) {
        return $title;
    }

    return generate_title_from_content( $post_id );
}

function yoast_title( $seo_title ) {
    global $post;

    if ( ! is_singular() ) {
        return $seo_title;
    }

    if ( is_user_title() ) {
        return $seo_title;
    }

    return str_replace( $post->post_title, generate_title_from_content( $post->ID ), $seo_title );
}

function is_uid_title( $title, $post_id ) {
    return empty( get_post_meta( $post_id, "_msgwp_$title", true ) ) ? false : true;
}

function is_user_title() {
    global $post;

    return ! is_uid_title( $post->post_title, $post->ID );
}

function generate_title_from_content( $post_id ) {
    $content = wp_strip_all_tags( get_post_field( 'post_content', $post_id ) );

    $title = wp_trim_words( $content, apply_filters( 'msgwp/addon/theme_compat/title/max_words', 3 ), '' );
    $title = truncate_phrase( $title, apply_filters( 'msgwp/addon/theme_compat/title/max_chars', 25 ) );
    $title .= __( '&hellip;' );

    return apply_filters( 'msgwp/addon/theme_compat/title', $title, $post_id );
}

function truncate_phrase( $text, $max_characters ) {
    if ( mb_strlen( $text ) > $max_characters ) {
        $text      = mb_substr( $text, 0, $max_characters + 1 );
        $text_trim = trim( mb_substr( $text, 0, mb_strrpos( $text, ' ' ) ) );
        $text      = empty( $text_trim ) ? $text : $text_trim;
    }

    return $text;
}

if ( class_exists( 'Puc_v4_Factory' ) ) {
    $update_checker = Puc_v4_Factory::buildUpdateChecker(
        'https://gitlab.com/msgwp/msgwp-theme-compat/',
        __FILE__,
        'msgwp-theme-compat'
    );
}