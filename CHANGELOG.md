All notable changes to this project will be documented in this file.

### 0.10.0 (2019-02-13)

#### Added
- `post-msgwp` CSS class for the `post_class`
- Support for Yoast SEO default configuration

### 0.8.0 (2019-02-06)

#### Added
- Before activation we check if msgWP (core) is installed and activated.